# GLUG Task Ans.

1. 
  * mkdir Music
  * touch song{1..10}.mp3
  * mv song*.mp3 ./Music
  * ln -s Music Audio


2. 
  * sudo su
  * groupadd sysadmin
  * groupadd manager
  * useradd  bob
  * useradd  rob
  * useradd  max
  * echo -e "linux123\nlinux123" | passwd bob
  * echo -e "linux123\nlinux123" | passwd rob
  * echo -e "linux123\nlinux123" | passwd max
  * usermod -g sysadmin bob
  * usermod -g sysadmin rob
  * usermod -g sysadmin max
  * usermod -G manager bob
  * usermod -G manager rob


3. 
  * `vim /etc/passwd`
    and edit `max:x:1003:1005::/home/max:/bin/bash`
    to `max:x:1003:1005::/home/max:/bin/false`
  * sudo chage -E \`date -d "30 days" +"%Y-%m-%d"\` max
  * chage -d 0 bob

4. 
  * mkdir /home/manager
  * chgrp manager /home/manager
  * chmod -R 2770 /home/manager


5. 
  * useradd -u 4223 gabriel


6. 
  * `mkdir /tmp/var`
   and `tar -cvjf /tmp/var/archive.tar.bz2 /etc/hosts`


7.   
  * cat /etc/services | grep udp > udp_services.txt


8. 
  * `top` then `shift+f` then `s` to set the sort then `esc` to go back to the main screen of top


9. 
  * `echo 'alias stat="/bin/uptime"' >> ~/.zshrc`
    and `source ~/.zshrc`


10. 
  * `vim blabla.txt` and `i` to insert 'esc' to go back to command mode and
      * `:wq` to save and exit ':q' to exit.
      * `:x` save and exit
      * `shift+ZZ` save and exit
      * `shift+ZQ` exit without saving
      * `:q!` exit without save


11.  
  * install firewalld
  * firewall-cmd --get-default-zone
  * firewall-cmd --set-default-zone=dmz
  * firewall-cmd --get-zones > zones.txt


12. 
  * `firewall-cmd --permanent --zone=public --add-port=8080/tcp`
  or `firewall-cmd --permanent --add-port=8080/tcp` to add to default zone.
  * firewall-cmd --list-ports >> zones.txt


13. 
  * `firewall-cmd --zone=public --permanent --add-forward-port=port=80:proto=tcp:toport=8080`
  * with iptables
      * `iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080`
